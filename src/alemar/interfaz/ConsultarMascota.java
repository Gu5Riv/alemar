/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Mascota;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Gu5Riv
 */
public class ConsultarMascota extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    
    //Para guardar el id de las mascota
    private int id;
    
    public ConsultarMascota() {
        initComponents();
        
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
        
        //Por default estaran desabilitados por si quiere ingresar datos
        //sin haber seleccionado una raza

        txtTipo.setEditable(false);
        txtPrecio.setEditable(false);
        txtTipo.setText("");
        txtPrecio.setText("");

        
        //Agregando mensajes de ayuda para el llenado de los campos
        cmbRazaAnimal.setToolTipText("Selecciona raza de animal.");

    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbRazaAnimal = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnConsultar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtTipo = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Consultar información de Mascotas");

        cmbRazaAnimal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbRazaAnimal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbRazaAnimalItemStateChanged(evt);
            }
        });
        cmbRazaAnimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbRazaAnimalActionPerformed(evt);
            }
        });

        jLabel2.setText("Raza del Animal");

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        jLabel4.setText("Precio");

        jLabel5.setText("Tipo");

        txtTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTipoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(120, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnConsultar)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 546, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(79, 79, 79))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel5))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(cmbRazaAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(183, 183, 183)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(242, 242, 242))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(jLabel1)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbRazaAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
                .addComponent(btnConsultar)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbRazaAnimalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbRazaAnimalItemStateChanged
             
    }//GEN-LAST:event_cmbRazaAnimalItemStateChanged

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        
        List<Mascota> LMascota = null;
        
        //Se extrae la raza seleccionada
        String raza = cmbRazaAnimal.getSelectedItem().toString();
        
        if(!(raza.equalsIgnoreCase("<-- Seleccione una opción -->"))){
                        
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Mascota AS p WHERE p.raza = :raza";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("raza", raza);
                    
                    //Se pone en una lista el registro sacado
                    LMascota = query.list();
                    
                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    txtTipo.setEditable(true);
                    txtPrecio.setEditable(true);
                    
                    txtTipo.setText(LMascota.get(0).getTipo());
                    String PrecioString = String.valueOf(LMascota.get(0).getPrecio());
                    txtPrecio.setText(PrecioString);


                    txt.commit();
                    
                    }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                }
        }else{
            cmbRazaAnimal.setSelectedIndex(0);
            txtTipo.setEditable(false);
            txtPrecio.setEditable(false);
        }
        
        
//        //Variable que maneja la respuesta del JOptionpane
//        int conf;
//             
//        //Extrayendo la informacion de los campos de texto
//        String tipo = txtTipo.getText();
//        String precioString = txtPrecio.getText();
//                
//        //Validaciones
//        if(!(tipo.isEmpty() || precioString.isEmpty())){ //Si no esta vacio algun campo           
//        
//            //Chunche de Hibernate (Inicio de sesion de hibernate)
//            Session session = null;
//            float precio = Float.parseFloat(precioString);
//            
//            try{
//                try{
//                    //Conexion a la BD
//                    sessionFactory = HibernateConexion.getSessionFactory();
//                    //Apertura de la sesion
//                    session = sessionFactory.openSession();
//                    Transaction tx = session.beginTransaction();                    
//  
//                    //Extrayendo el regitro de la raza seleccionada
//                    //en base al ID guardado previamente de manera global
//                    Mascota mascota = (Mascota)session.get(Mascota.class, id);
//                    //Ingresando la nueva informacion al objeto mascota
//
//                    mascota.setTipo(tipo);
//                    mascota.setPrecio(precio);
//
//                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
//                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro de ingresar esos datos?","Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
//                    
//                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI
//                        
//                        //Actualiza los datos del proveedor
//                        session.update(mascota);
//                       
//                        tx.commit();
//                        //Mensaje de confirmacion de insercion Exitosa
//                        JOptionPane.showMessageDialog(null,"Registro guardado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
//                        
//                        //cmbSexo.setEnabled(false);
//                        //cmbSexo.setSelectedIndex(0);
//   
//                    }
//                    else{
//                        //cmbSexo.setSelectedIndex(0);
//                        cmbRazaAnimal.setSelectedIndex(0);
//                        txtTipo.setText("");
//                        txtPrecio.setText("");
//                    }
//                }catch(Exception e){
//                    System.out.println(e.getMessage());
//                }
//            } finally {
//                //Finalizacion de la sesion de hibernate
//                session.close();
//                //Se vuelve a cargar el comboBox
//                cargarComboBox();
//        }
//      }
//        else {//Si hay campos vacios, mensaje de error
//            JOptionPane.showMessageDialog(null,"Error, no pueden haber campos vacios","Error en los datos",JOptionPane.ERROR_MESSAGE);
//            
//        } 
        
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void cmbRazaAnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbRazaAnimalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbRazaAnimalActionPerformed

    private void txtTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTipoActionPerformed

    //Metodo para cargar el comboBox dinamicamente de la BD
    private void cargarComboBox(){
        
        //Obtencion de todos los proveedores de la base de datos
        //para llenar el comboBox de manera dinamica
        
        //Variable que contendra la lista de proveedores
        List<Mascota> listaMascota = null;
        
        //Inicializacion de session de hibernate
        Session session = null;
        try{
            try{
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                
                //Obteniendo de la BD todos los proveedores                
                listaMascota = session.createQuery("from Mascota").list();
                
                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
                
                //Poniendole un mensaje de seleccion (Pirmer elemento)
                 modeloCombo.addElement("<-- Seleccione una opción -->");
                
                //Llenando el modelo de comboBox con los nombres de los proveedores
                for(Mascota p : listaMascota) 
                { 
                    modeloCombo.addElement(p.getRaza());                     
                }
                
                //Agregando el modelo del comboBox al comboBox del panel
                cmbRazaAnimal.setModel(modeloCombo);
                
            }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            }finally {
                session.close();
        }
        
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConsultar;
    private javax.swing.JComboBox cmbRazaAnimal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTipo;
    // End of variables declaration//GEN-END:variables
}
