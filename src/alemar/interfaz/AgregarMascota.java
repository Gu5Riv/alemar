package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Mascota;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class AgregarMascota extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;

    public AgregarMascota() {
        initComponents();

        txtTipo.setToolTipText("Ingrese tipo de animal. \n Ejemplo: Perro, Gato, Pez, etc.");
        txtRaza.setToolTipText("Ingrese raza del tipo de animal.");
        txtPrecio.setToolTipText("Ingrese precio del animal.");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAgregarEstante = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        txtRaza = new javax.swing.JTextField();
        txtTipo = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        btnAgregarEstante.setText("Agregar");
        btnAgregarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarEstanteActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Agregar nueva Mascota al sistema");

        jLabel2.setText("Tipo");

        jLabel3.setText("Raza");

        jLabel5.setText("Precio");

        txtPrecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioKeyTyped(evt);
            }
        });

        txtRaza.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRazaKeyTyped(evt);
            }
        });

        txtTipo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTipoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(241, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAgregarEstante)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(245, 245, 245))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtPrecio)
                            .addComponent(txtRaza)
                            .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(217, 217, 217))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(jLabel1)
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtRaza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarEstante)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarEstanteActionPerformed
        // TODO add your handling code here:

        //Variable que maneja la respuesta del JOptionPane
        int conf;

        //Extrayendo la informacion de los campos de texto
        String tipo = txtTipo.getText();
        String raza = txtRaza.getText();
        //String sexo = cmbSexo.getSelectedItem().toString();
        String precioString = txtPrecio.getText();

        //Validaciones
        if (!(tipo.isEmpty() || raza.isEmpty() || precioString.isEmpty())) { //Si no esta vacio algun campo           
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            float precio = Float.parseFloat(precioString);
            
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Creando el objeto Proveedor 
                    Mascota mascota = new Mascota();
                    //Ingresando la informacion al objeto mascota
                    mascota.setRaza(raza);
                    mascota.setPrecio(precio);
                    mascota.setTipo(tipo);

                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar estos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI
                        if (!(verificarMascota(raza))) {
                            //Guardando el objeto en la base de datos
                            session.save(mascota);
                            tx.commit();
                            //Mensaje de confirmacion de insercion Exitosa
                            JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);

                            txtTipo.setText("");
                            txtRaza.setText("");
                            txtPrecio.setText("");
                        } else {
                            JOptionPane.showMessageDialog(null, "Error, Raza ya existente", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                            //Se resetean los capos de texto y los comboBox
                            txtTipo.setText("");
                            txtRaza.setText("");
                            txtPrecio.setText("");
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnAgregarEstanteActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed

        //Se resetean los capos de texto y los comboBox
        txtTipo.setText("");
        txtRaza.setText("");
        txtPrecio.setText("");

    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void txtPrecioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyTyped
        // SOLO NUMEROS y punto
        char c;
        //capturar el caracter digitado
        c = evt.getKeyChar();
        if ((c < '0' || c > '9')  && (c != '.')) {
            evt.consume();//ignora el caracter digitado 
        }
    }//GEN-LAST:event_txtPrecioKeyTyped

    private void txtTipoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTipoKeyTyped
        //Solo permite escribir letras
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }
    }//GEN-LAST:event_txtTipoKeyTyped

    private void txtRazaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRazaKeyTyped
        //Solo permite escribir letras
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }
    }//GEN-LAST:event_txtRazaKeyTyped

    //Metodo para verificar la existencia de un proveedor
    private boolean verificarMascota(String raza) {

        //Variable que contendra la lista de proveedores
        List<Mascota> listaMascota = null;

        boolean resultado;

        //Chunche de Hibernate (Inicio de sesion de hibernate)
        Session session = null;
        try {
            try {
                //Conexion a la BD
                sessionFactory = HibernateConexion.getSessionFactory();
                //Apertura de la sesion
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Obteniendo de la BD todos los estantes                
                listaMascota = session.createQuery("from Mascota").list();

                //Verificando los codigos de los estantes
                for (Mascota e : listaMascota) {
                    if (raza.equals(e.getRaza())) {
                        return resultado = true;
                    }

                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } finally {
            //Finalizacion de la sesion de hibernate
            session.close();
        }

        return resultado = false;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarEstante;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtRaza;
    private javax.swing.JTextField txtTipo;
    // End of variables declaration//GEN-END:variables
}
