/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Mascota;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Gu5Riv
 */
public class ActualizarMascota extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    
    //Para guardar el id de las mascota
    private int id;
    
    public ActualizarMascota() {
        initComponents();
        
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
        
        //Por default estaran desabilitados por si quiere ingresar datos
        //sin haber seleccionado una raza
        txtTipo.setEditable(false);
        txtPrecio.setEditable(false);
        
        //Agregando mensajes de ayuda para el llenado de los campos
        txtTipo.setToolTipText("Ingrese tipo de animal. \n Ejemplo: Perro, Gato, Pez, etc.");
        cmbRazaAnimal.setToolTipText("Selecciona raza de animal.");
        txtPrecio.setToolTipText("Ingrese precio del animal.");
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbRazaAnimal = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnActualizarEstante = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtTipo = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Actualizar información de Mascotas");

        cmbRazaAnimal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbRazaAnimal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbRazaAnimalItemStateChanged(evt);
            }
        });

        jLabel2.setText("Raza del Animal");

        btnActualizarEstante.setText("Actualizar");
        btnActualizarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarEstanteActionPerformed(evt);
            }
        });

        jLabel4.setText("Precio");

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jLabel5.setText("Tipo");

        txtTipo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTipoKeyTyped(evt);
            }
        });

        txtPrecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(120, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarEstante)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addGap(18, 18, 18)
                                    .addComponent(cmbRazaAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(158, 158, 158))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel5))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtTipo, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                                        .addComponent(txtPrecio))
                                    .addGap(157, 157, 157))))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 546, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(79, 79, 79)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(242, 242, 242))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(jLabel1)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbRazaAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarEstante)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbRazaAnimalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbRazaAnimalItemStateChanged
        
    
        List<Mascota> LMascota = null;
        
        //Se extrae la raza seleccionada
        String raza = cmbRazaAnimal.getSelectedItem().toString();
        
        if(!(raza.equalsIgnoreCase("<-- Seleccione una opción -->"))){
                        
            //Inicio de sesion de hibernate
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Mascota AS p WHERE p.raza = :raza";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos de mascota de la BD en base a la raza                   
                    Query query = session.createQuery(hql).setString("raza", raza);
                    
                    //Se pone en una lista el registro sacado
                    LMascota = query.list();
                    
                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    txtTipo.setEditable(true);
                    txtPrecio.setEditable(true);
                    
                    txtTipo.setText(LMascota.get(0).getTipo());
                    String PrecioString = String.valueOf(LMascota.get(0).getPrecio());
                    txtPrecio.setText(PrecioString);


                    txt.commit();
                    
                    }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                }
        }else{
            cmbRazaAnimal.setSelectedIndex(0);
            txtTipo.setEditable(false);
            txtPrecio.setEditable(false);
        }
    }//GEN-LAST:event_cmbRazaAnimalItemStateChanged

    private void btnActualizarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarEstanteActionPerformed
        
        String tipo;
        String precioString;

        //Variable que maneja la respuesta del JOptionpane
        int conf;

        //Extrayendo la informacion de los campos de texto
        tipo = txtTipo.getText();
        precioString = txtPrecio.getText();

        //Validaciones
        if (!(tipo.isEmpty() || precioString.isEmpty())) { //Si no esta vacio algun campo           

            float precio = Float.parseFloat(precioString);
            System.out.println("Hace el Parse");
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();
                    System.out.println("Abre session");
                    //Extrayendo el regitro del proveedor seleccionado
                    //en base al ID guardado previamente de manera global
                    Mascota mascota = (Mascota) session.get(Mascota.class, id);

                    //Ingresando la nueva informacion del proveedor al  objeto Proveedor
                    mascota.setTipo(tipo);
                    mascota.setPrecio(precio);
                    System.out.println("Mete nuevos datos al objeto");


                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de actualizar estos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {
                        System.out.println("Entra al if del SI del JOptionPane");
                         //Guardando el objeto en la base de datos
                            session.update(mascota);
                            tx.commit();
                             //Finalizacion de la sesion de hibernate
                                session.close();
                                
                                //Limpiamos todos los campos
                                limpiarCampos();
                                //Poner en el primer item el comboBox
                            cmbRazaAnimal.setSelectedIndex(0);

                            //Mensaje de confirmacion de insercion Exitosa
                            JOptionPane.showMessageDialog(null, "Registro actualizado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                                                       
                            }
                       }
                catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos: ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
                

                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "El tipo: " + tipo + " ya se encuentra asignado ,\nPor Favor Ingrese otro tipo de animal", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtTipo.setText("");

                }      
             finally {
                 //Se vuelve a cargar el comboBox
                cargarComboBox();
                
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }
   
    }//GEN-LAST:event_btnActualizarEstanteActionPerformed

        private void limpiarCampos() {
        txtTipo.setText("");
        txtPrecio.setText("");
    }
    
    
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        
            cmbRazaAnimal.setSelectedIndex(0);
            txtTipo.setEditable(false);
            txtTipo.setText("");
            txtPrecio.setEditable(false);
            txtPrecio.setText("");
        
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtTipoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTipoKeyTyped
        //Solo permite escribir letras
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }
    }//GEN-LAST:event_txtTipoKeyTyped

    private void txtPrecioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyTyped
        // SOLO NUMEROS y punto
        char c;
        //capturar el caracter digitado
        c = evt.getKeyChar();
        if ((c < '0' || c > '9')  && (c != '.')) {
            evt.consume();//ignora el caracter digitado 
        }
    }//GEN-LAST:event_txtPrecioKeyTyped

    //Metodo para cargar el comboBox dinamicamente de la BD
    private void cargarComboBox(){
        
        //Obtencion de todos los proveedores de la base de datos
        //para llenar el comboBox de manera dinamica
        
        //Variable que contendra la lista de proveedores
        List<Mascota> listaMascota = null;
        
        //Inicializacion de session de hibernate
        Session session = null;
        try{
            try{
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                
                //Obteniendo de la BD todos los proveedores                
                listaMascota = session.createQuery("from Mascota").list();
                
                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
                
                //Poniendole un mensaje de seleccion (Pirmer elemento)
                 modeloCombo.addElement("<-- Seleccione una opción -->");
                
                //Llenando el modelo de comboBox con los nombres de los proveedores
                for(Mascota p : listaMascota) 
                { 
                    modeloCombo.addElement(p.getRaza());                     
                }
                
                //Agregando el modelo del comboBox al comboBox del panel
                cmbRazaAnimal.setModel(modeloCombo);
                
            }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            }finally {
                session.close();
        }
        
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarEstante;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox cmbRazaAnimal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTipo;
    // End of variables declaration//GEN-END:variables
}
