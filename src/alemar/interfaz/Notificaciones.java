package alemar.interfaz;

import alemar.entidad.Producto;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import alemar.configuracion.HibernateConexion;

/**
 *
 * @author Gus
 */
public class Notificaciones extends javax.swing.JDialog {

    
    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Para guardar el id del proveedor
    private int id;

    public boolean flag;
    public boolean flag2;
    /**
     * Creates new form Notificaciones
     */
    public Notificaciones(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        JDialog.setDefaultLookAndFeelDecorated(true);
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.OfficeSilver2007Skin");
       
        initComponents();
        super.setLocation(666, 100);

        cargarListaNotificaciones();
    }

    Notificaciones(){
        //Constructor vacio
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnOcultarNotificaciones = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstNotificaciones = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstNotificacionesVencimiento = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(400, 271));
        setMinimumSize(new java.awt.Dimension(400, 271));
        setUndecorated(true);
        setResizable(false);

        btnOcultarNotificaciones.setText("Ocultar");
        btnOcultarNotificaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOcultarNotificacionesActionPerformed(evt);
            }
        });

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        lstNotificaciones.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(lstNotificaciones);

        lstNotificacionesVencimiento.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(lstNotificacionesVencimiento);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 248, Short.MAX_VALUE)
                .addComponent(btnActualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOcultarNotificaciones))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOcultarNotificaciones)
                    .addComponent(btnActualizar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOcultarNotificacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOcultarNotificacionesActionPerformed
        dispose();
    }//GEN-LAST:event_btnOcultarNotificacionesActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        cargarListaNotificaciones();
    }//GEN-LAST:event_btnActualizarActionPerformed

   public void cargarListaNotificaciones() {
       
        //Obtencion de todos las notificaciones de la base de datos
        //para llenar la lista de manera dinamica
        //Variable que contendra la lista del resultado del query
       List<Producto> listaNotif = null;
       List<Producto> listaNotifVencimiento = null;

        //Inicializacion de session de hibernate
        Session session = null;
        try {
            try {
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Obteniendo de la BD todos los proveedores                
                listaNotif = session.createQuery("from Producto where cantidad <= limiteProducto").list();
                listaNotifVencimiento = session.createQuery("from Producto where limiteActivo = 1 and cantidad > 0").list();
                
                //Creando un modelo de comboBox                
                DefaultListModel modeloLista = new DefaultListModel();
                DefaultListModel modeloLista2 = new DefaultListModel();

                for(Producto b : listaNotif){
                    modeloLista.addElement("El producto "+ b.getNombreProducto() + " ha llegado al límite mínimo.");
                }
                
                for(Producto a : listaNotifVencimiento){
                    modeloLista2.addElement("El producto "+ a.getNombreProducto() + " se acerca a la fecha de su vencimiento.");
                }

                //Agregando el modelo del comboBox al comboBox del panel
                lstNotificaciones.setModel(modeloLista);
                lstNotificacionesVencimiento.setModel(modeloLista2);

                flag = listaNotif.isEmpty();
                flag2 = listaNotifVencimiento.isEmpty();
                
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } finally {
            session.close();
        }

    }
    
    public boolean estadoNotif() {
        return (!flag || !flag2);
    }
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnOcultarNotificaciones;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList lstNotificaciones;
    private javax.swing.JList lstNotificacionesVencimiento;
    // End of variables declaration//GEN-END:variables
}
